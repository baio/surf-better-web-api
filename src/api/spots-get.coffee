"use strict"

elasticsearch = require "elasticsearch"

config = require("yaml-config").readConfig('./configs.yml', process.env.NODE_ENV).elasticsearch
config ?= host : process.env.BONSAI_URL
client = new elasticsearch.Client config
Q = require "q"

module.exports = (geo, term) ->
  if geo
    q =
      sort : [
        "_score",
        {
          _geo_distance :
            geo :
              lat : geo[0]
              lon : geo[1]
            order : "asc",
            unit : "km"
        }
    ]

  if term
    matches = [{field : "country", boost : 1}, {field : "region", boost : 2}, {field : "city", boost : 3}, {field : "spot", boost : 5}].map (m) ->
      res = match :  {}
      res.match["#{m.field}.autocomplete"] = {"query" : term, "fuzziness": "AUTO", "boost" : m.boost}
      res
    q ?= {}
    q.query =
      function_score :
        functions : [
          script_score :
           script : if geo then "doc['geo'].distanceInKm(#{geo[0]}, #{geo[1]}) < 50 ? _score * 1000 : _score" else "_score"
        ]
        query :
          bool : should : matches
    q.highlight =  fields : {"spot.autocomplete" : {}, "region.autocomplete" : {}, "city.autocomplete" : {}, "country.autocomplete" : {}}

  if q
    s =
      index: "spots"
      type: "spot"
      body : q
    client.search(s).then (res) ->
      res.hits.hits.map (m) ->
        r = m._source
        r.id = m._id
        if m.sort
          r.dist = Math.round m.sort[1]
        r.city = null if !r.city
        r.region = null if !r.region
        r.country = null if !r.country
        parts = [r.city, r.region, r.country].filter((f) -> f)
        r.label = [r.spot, parts[0]].join(", ")
        if m.highlight
          for own p of m.highlight
            r[p.replace(".autocomplete", "")] = m.highlight[p][0]
        r
  else
    Q([])

