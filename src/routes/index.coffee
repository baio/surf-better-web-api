"use strict"

module.exports = [
  require("./forecast/msw-forecast-get")
  require("./report/report-post")
  require("./report/report-get")
  require("./report/report-message-post")
  require("./spot/spots-get")
  require("./instant-report/msw-instatnt-report-get")
  require("./user/login")
]