module.exports = {
  "_id" : "BS_NP_NA_WI",
  "name" : "Willawahs",
  "location" : {
    "country" : {
      "code" : "BS",
      "name" : "The Bahamas"
    },
    "region" : {
      "code" : "BS_NP",
      "name" : "New Providence"
    },
    "city" : {
      "code" : "BS_NP_NA",
      "name" : "Nassau"
    },
    "geo" : [
      25.0497768,
      -77.32358720000001
    ]
  },
  "tz" : "America/Nassau",
  "best" : {
    "swell" : "NE",
    "wind" : "SW"
  },
  "season" : "consistent",
  "issuers" : [
    {
      "name" : "surf-forecast",
      "code" : "Willawahs"
    }
  ]
}