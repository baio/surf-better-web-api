module.exports =
{
"spot" : "BS_NP_NA_WI",
"tz" : "America/Nassau",
"issuer" : {
  "code" : "surf-forecast",
  "datesRange" : {
    "from" : new Date("2014-09-05T02:00:00.000Z"),
    "to" : new Date("2014-09-06T23:00:00.000Z")
  },
  "readDateTime" : new Date("2014-09-05T17:40:09.539Z"),
  "issuerDateTime" : new Date("2014-09-05T11:00:00.000Z"),
},
"conditions" : [
  {
    "dateTime" : new Date("2014-09-05T02:00:00.000Z"),
    "weather" : "some clouds",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross"
      },
      "speed" : {
        "steady" : 25
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-05T02:00:00.000Z"),
    "weather" : "clear",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "SE",
        "shore" : "cross"
      },
      "speed" : {
        "steady" : 20
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-05T05:00:00.000Z"),
    "weather" : "some clouds",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross"
      },
      "speed" : {
        "steady" : 25
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 1.1,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-05T08:00:00.000Z"),
    "weather" : "some clouds",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross-on"
      },
      "speed" : {
        "steady" : 25
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 1.1,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-05T11:00:00.000Z"),
    "weather" : "clear",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross-on"
      },
      "speed" : {
        "steady" : 20
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 1.1,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-05T14:00:00.000Z"),
    "weather" : "clear",
    "temperature" : {
      "air" : 29
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross-on"
      },
      "speed" : {
        "steady" : 20
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-05T17:00:00.000Z"),
    "weather" : "clear",
    "temperature" : {
      "air" : 29
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross-on"
      },
      "speed" : {
        "steady" : 20
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 1,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-05T20:00:00.000Z"),
    "weather" : "clear",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross-on"
      },
      "speed" : {
        "steady" : 25
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-05T23:00:00.000Z"),
    "weather" : "clear",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross"
      },
      "speed" : {
        "steady" : 20
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-06T05:00:00.000Z"),
    "weather" : "clear",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross"
      },
      "speed" : {
        "steady" : 25
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-06T08:00:00.000Z"),
    "weather" : "clear",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross"
      },
      "speed" : {
        "steady" : 20
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-06T11:00:00.000Z"),
    "weather" : "clear",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross-on"
      },
      "speed" : {
        "steady" : 20
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  },
  {
    "dateTime" : new Date("2014-09-06T14:00:00.000Z"),
    "weather" : "rain shwrs",
    "temperature" : {
      "air" : 29
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross-on"
      },
      "speed" : {
        "steady" : 20
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : 1
    }
  },
  {
    "dateTime" : new Date("2014-09-06T17:00:00.000Z"),
    "weather" : "rain shwrs",
    "temperature" : {
      "air" : 29
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross-on"
      },
      "speed" : {
        "steady" : 25
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : 1
    }
  },
  {
    "dateTime" : new Date("2014-09-06T20:00:00.000Z"),
    "weather" : "rain shwrs",
    "temperature" : {
      "air" : 29
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross"
      },
      "speed" : {
        "steady" : 25
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : 1
    }
  },
  {
    "dateTime" : new Date("2014-09-06T23:00:00.000Z"),
    "weather" : "some clouds",
    "temperature" : {
      "air" : 28
    },
    "wind" : {
      "direction" : {
        "compass" : "ESE",
        "shore" : "cross"
      },
      "speed" : {
        "steady" : 25
      }
    },
    "swell" : {
      "direction" : {
        "compass" : "ESE"
      },
      "height" : 0.9,
      "period" : 7
    },
    "custom" : {
      "rating" : 0,
      "vawes_energy" : 7,
      "rain" : null
    }
  }
]
}