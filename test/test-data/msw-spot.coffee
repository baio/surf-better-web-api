module.exports =
{
  "_id" : "AU_QLD_MA_PCM",
  "name" : "Pin Cushion (Maroochydore)",
  "location" : {
    "country" : {
      "code" : "AU",
      "name" : "Australia"
    },
    "region" : {
      "code" : "AU_QLD",
      "name" : "Queensland"
    },
    "city" : {
      "code" : "AU_QLD_MA",
      "name" : "Maroochydore"
    },
    "tz" : "Australia/Brisbane",
    "geo" : [
      -26.6607,
      153.106
    ]
  },
  "issuers" : [
    {
      "name" : "msw",
      "code" : "1005"
    }
  ]
}